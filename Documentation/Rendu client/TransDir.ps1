﻿#Cartouche
#Auteur : Théo Esseiva
#Version 1.0
#Script permettant de migrer une arborescence sur la machine de quelqun suivant sont domaine d'application


#Function pour l'export des fichiers vers le dossier de partage
Function Button_Export()
{
    #Condition si le permier bouton radio est séléctionné
    if ($RadioButton1.Checked)
    {
        #Condition si il existe des dossiers déjà existant dans le dossier partagé
        if( (Get-ChildItem C:\Partage).Count -eq 0)
        {
            #Copie de tous les dossiers par rapport à l'utilisateur séléctionné
            Copy-Item -Path C:\Arborescence\Direction -Recurse -Destination C:\Partage -Force
            Copy-Item -Path C:\Arborescence\Projet\Comptabilité-Direction -Recurse -Destination C:\Partage -Force
            Copy-Item -Path C:\Arborescence\Commun -Recurse -Destination C:\Partage -Force
            [System.Windows.Forms.MessageBox]::Show("Export pour utilisateur direction effectué" , "FYI")
         } 
         else
         {      
            #Message qui demande à l'utilisateur si il est sur de vouloir écrire par dessus les dossiers déjà dans le dossier de partage   
            $MsgBoxInput = [System.Windows.MessageBox]::Show('Êtes-vous sur de vouloir tout remplacer ?', 'Advertissement','YesNoCancel')

            #Tableau contenant les différentes possibilités de réponses
            switch ($msgBoxInput){
                 #Si l'utilisateur choisi "yes", le script supprime tous les dossiers dans le dossier de partages et copies les dossiers par rapport à l'utilisateur séléctionné
                 'Yes' {    
                                Get-ChildItem C:\Partage | % {Remove-Item -Path $_.FullName -Recurse}

                                Copy-Item -Path C:\Arborescence\Direction -Recurse -Destination C:\Partage -Force
                                Copy-Item -Path C:\Arborescence\Projet\Comptabilité-Direction -Recurse -Destination C:\Partage -Force
                                Copy-Item -Path C:\Arborescence\Commun -Recurse -Destination C:\Partage -Force

                                [System.Windows.Forms.MessageBox]::Show("Export pour utilisateur direction updaté" , "FYI")
                       }
                #Si l'utilisateur choisi "No", le script affiche juste un message de confirmation de l'annulation
                'No'   {
                    
                            [System.Windows.Forms.MessageBox]::Show("Vous avez annuler le remplacement" , "FYI")

                       }
                #Si l'utilisateur choisi "Cancel", le script ne fait rien du tout
                'Cancel'{}

         }
        }
     #Condition si le deuxième bouton radio est séléctionné
    }elseif($RadioButton2.Checked)
    {
        #Condition si il existe des dossiers déjà existant dans le dossier partagé
        if( (Get-ChildItem C:\Partage).Count -eq 0)
         {
            #Copie de tous les dossiers par rapport à l'utilisateur séléctionné
            Copy-Item -Path C:\Arborescence\Cadre -Recurse -Destination C:\Partage -Force
            Copy-Item -Path C:\Arborescence\Projet\Vente-Cadre -Recurse -Destination C:\Partage -Force
            Copy-Item -Path C:\Arborescence\Commun -Recurse -Destination C:\Partage -Force
            [System.Windows.Forms.MessageBox]::Show("Export pour utilisateur cadre effectué" , "FYI")
            
         } 
         else 
         {
            #Message qui demande à l'utilisateur si il est sur de vouloir écrire par dessus les dossiers déjà existant dans le dossier de partage
            $MsgBoxInput = [System.Windows.MessageBox]::Show('Êtes-vous sur de vouloir tout remplacer ?', 'Advertissement','YesNoCancel')

            #Tableau contenant les différentes possibilités de réponses
            switch ($msgBoxInput){
                    #Si l'utilisateur choisi "yes", le script supprime tous les dossiers dans le dossier de partages et copies les dossiers par rapport à l'utilisateur séléctionné
                    'Yes' {     
                                Get-ChildItem C:\Partage | % {Remove-Item -Path $_.FullName -Recurse}

                                Copy-Item -Path C:\Arborescence\Cadre -Recurse -Destination C:\Partage -Force
                                Copy-Item -Path C:\Arborescence\Projet\Vente-Cadre -Recurse -Destination C:\Partage -Force
                                Copy-Item -Path C:\Arborescence\Commun -Recurse -Destination C:\Partage -Force

                                [System.Windows.Forms.MessageBox]::Show("Export pour utilisateur cadre updaté" , "FYI")
                         }
                    #Si l'utilisateur choisi "No", le script affiche juste un message de confirmation de l'annulation
                    'No'  {
                                [System.Windows.Forms.MessageBox]::Show("Vous avez annuler le remplacement" , "FYI")
                          }
                    #Si l'utilisateur choisi "Cancel", le script ne fait rien du tout
                    'Cancel'  {}
                                  }       
        }
    }
    #Condition si le troisième bouton radio est séléctionné
    elseif($RadioButton3.Checked)
    {
        #Condition si il existe des dossiers déjà existant dans le dossier partagé
        if( (Get-ChildItem C:\Partage).Count -eq 0)
         {
            #Copie de tous les dossiers par rapport à l'utilisateur séléctionné
            Copy-Item -Path C:\Arborescence\Vente -Recurse -Destination C:\Partage -Force
            Copy-Item -Path C:\Arborescence\Projet\Vente-Cadre -Recurse -Destination C:\Partage -Force
            Copy-Item -Path C:\Arborescence\Commun -Recurse -Destination C:\Partage -Force
            [System.Windows.Forms.MessageBox]::Show("Export pour utilisateur cadre effectué" , "FYI")
            
         } 
         else 
         {
            #Message qui demande à l'utilisateur si il est sur de vouloir écrire par dessus les dossiers déjà existant dans le dossier de partage
            $MsgBoxInput = [System.Windows.MessageBox]::Show('Êtes-vous sur de vouloir tout remplacer ?', 'Advertissement','YesNoCancel')

            #Tableau contenant les différentes possibilités de réponses
            switch ($msgBoxInput){
                    #Si l'utilisateur choisi "yes", le script supprime tous les dossiers dans le dossier de partages et copies les dossiers par rapport à l'utilisateur séléctionné
                    'Yes' {        
                                Get-ChildItem C:\Partage | % {Remove-Item -Path $_.FullName -Recurse}

                                Copy-Item -Path C:\Arborescence\Vente -Recurse -Destination C:\Partage -Force
                                Copy-Item -Path C:\Arborescence\Projet\Vente-Cadre -Recurse -Destination C:\Partage -Force
                                Copy-Item -Path C:\Arborescence\Commun -Recurse -Destination C:\Partage -Force
                                [System.Windows.Forms.MessageBox]::Show("Export pour utilisateur vente updaté" , "FYI")
                          }
                    #Si l'utilisateur choisi "No", le script affiche juste un message de confirmation de l'annulation
                    'No'  {
                                [System.Windows.Forms.MessageBox]::Show("Vous avez annuler le remplacement" , "FYI")
                          }
                    #Si l'utilisateur choisi "Cancel", le script ne fait rien du tout
                    'Cancel'  {}
                                }       
        }      
    }
    #Condition si le quatrième bouton radio est séléctionné
    elseif($RadioButton4.Checked)
    {
        #Condition si il existe des dossiers déjà existant dans le dossier partagé 
        if( (Get-ChildItem C:\Partage).Count -eq 0)
         {
            #Copie de tous les dossiers par rapport à l'utilisateur séléctionné
            Copy-Item -Path C:\Arborescence\Administration -Recurse -Destination C:\Partage -Force         
            Copy-Item -Path C:\Arborescence\Commun -Recurse -Destination C:\Partage -Force
            [System.Windows.Forms.MessageBox]::Show("Export pour utilisateur administration effectué" , "FYI")
         } 
         else 
         {
            #Message qui demande à l'utilisateur si il est sur de vouloir écrire par dessus les dossiers déjà existant dans le dossier de partage
            $MsgBoxInput = [System.Windows.MessageBox]::Show('Êtes-vous sur de vouloir tout remplacer ?', 'Advertissement','YesNoCancel')

            #Tableau contenant les différentes possibilités de réponses             
            switch ($msgBoxInput){
                    #Si l'utilisateur choisi "yes", le script supprime tous les dossiers dans le dossier de partages et copies les dossiers par rapport à l'utilisateur séléctionné
                    'Yes' {        
                                Get-ChildItem C:\Partage | % {Remove-Item -Path $_.FullName -Recurse}

                                Copy-Item -Path C:\Arborescence\Administration -Recurse -Destination C:\Partage -Force
                                Copy-Item -Path C:\Arborescence\Commun -Recurse -Destination C:\Partage -Force
                                [System.Windows.Forms.MessageBox]::Show("Export pour utilisateur administration updaté" , "FYI")
                          }

                    #Si l'utilisateur choisi "No", le script affiche juste un message de confirmation de l'annulation
                    'No'  {
                                [System.Windows.Forms.MessageBox]::Show("Vous avez annuler le remplacement" , "FYI")

                          }
                    #Si l'utilisateur choisi "Cancel", le script ne fait rien du tout
                    'Cancel'  {}
                                 }       
        }      
    }
    #Condition si le cinquième bouton radio est séléctionné
    elseif($RadioButton5.Checked)
    {
        #Condition si il existe des dossiers déjà existant dans le dossier partagé 
        if( (Get-ChildItem C:\Partage).Count -eq 0)
         {
            #Copie de tous les dossiers par rapport à l'utilisateur séléctionné
            Copy-Item -Path C:\Arborescence\Compatibilté -Recurse -Destination C:\Partage -Force   
            Copy-Item -Path C:\Arborescence\Projet\Comptabilité-Direction -Recurse -Destination C:\Partage -Force 
            Copy-Item -Path C:\Arborescence\Commun -Recurse -Destination C:\Partage -Force
            [System.Windows.Forms.MessageBox]::Show("Export pour utilisateur comptabilté effectué" , "FYI")           
         } 
         else 
         {
            #Message qui demande à l'utilisateur si il est sur de vouloir écrire par dessus les dossiers déjà existant dans le dossier de partage
            $MsgBoxInput = [System.Windows.MessageBox]::Show('Êtes-vous sur de vouloir tout remplacer ?', 'Advertissement','YesNoCancel')

            #Tableau contenant les différentes possibilités de réponses
            switch ($msgBoxInput){
                     #Si l'utilisateur choisi "yes", le script supprime tous les dossiers dans le dossier de partages et copies les dossiers par rapport à l'utilisateur séléctionné
                    'Yes' {        
                                Get-ChildItem C:\Partage | % {Remove-Item -Path $_.FullName -Recurse}

                                Copy-Item -Path C:\Arborescence\Administration -Recurse -Destination C:\Partage -Force
                                Copy-Item -Path C:\Arborescence\Projet\Comptabilité-Direction -Recurse -Destination C:\Partage -Force 
                                Copy-Item -Path C:\Arborescence\Commun -Recurse -Destination C:\Partage -Force
                                [System.Windows.Forms.MessageBox]::Show("Export pour utilisateur comptabilté updaté" , "FYI")
                          }
                    #Si l'utilisateur choisi "No", le script affiche juste un message de confirmation de l'annulation
                    'No'  {
                                [System.Windows.Forms.MessageBox]::Show("Vous avez annuler le remplacement" , "FYI")
                          }
                    #Si l'utilisateur choisi "Cancel", le script ne fait rien du tout
                    'Cancel'  {}

                                }       
        }      
    }    
    #Condition si la checkbox pour les dossiers personnels est cochée ou pas
    if($Checkbox.Checked)
    {     
         Copy-Item -Path C:\Arborescence\Perso -Destination C:\Partage
    }      
}

#Fonction pour la restauration de l'arborescence du dossier de partage vers la machine cible
Function Button_Restore()
{
    #Récupération de tous les dossier que contient le dossier de partage
    Get-ChildItem \\MACHINE_MAITRE\Partage | % {Copy-Item -Path $_.FullName -Recurse -Destination C:\Arborescence -Force -Container }
    [System.Windows.Forms.MessageBox]::Show("Restore effectué" , "My Dialog Box")
}

#Ajout des librairies
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

#Initialisation du formulaire
$form = New-Object System.Windows.Forms.Form
$form.Text = 'TransDir'
$form.Size = New-Object System.Drawing.Size(500,350)
$form.StartPosition = 'CenterScreen'

#Supression du bouton d'agrandissement de la fenêtre
$form.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedDialog
$form.MaximizeBox = $False
$form.MinimizeBox = $False

#Création du label pour les radios buttons
$label = New-Object System.Windows.Forms.Label
$label.Location = New-Object System.Drawing.Point(10,15)
$label.Size = New-Object System.Drawing.Size(280,20)
$label.Text = 'Choix de classe de métier :'
$form.Controls.Add($label)

#Création du permier radio bouton
$RadioButton1 = New-Object System.Windows.Forms.RadioButton
$RadioButton1.Location =  New-Object System.Drawing.Size (20,50)
$RadioButton1.Size = New-Object System.Drawing.Size(350,20)
$RadioButton1.Text = 'Direction'
$form.Controls.Add($RadioButton1)

#Création du deuxième radio bouton
$RadioButton2 = New-Object System.Windows.Forms.RadioButton
$RadioButton2.Location =  New-Object System.Drawing.Size (20,75)
$RadioButton2.Size = New-Object System.Drawing.Size(350,20)
$RadioButton2.Text = 'Cadre'
$form.Controls.Add($RadioButton2)

#Création du troisième radio bouton
$RadioButton3 = New-Object System.Windows.Forms.RadioButton
$RadioButton3.Location =  New-Object System.Drawing.Size (20,100)
$RadioButton3.Size = New-Object System.Drawing.Size(350,20)
$RadioButton3.Text = 'Vente'
$form.Controls.Add($RadioButton3)

#Création du quatrième radio bouton
$RadioButton4 = New-Object System.Windows.Forms.RadioButton
$RadioButton4.Location =  New-Object System.Drawing.Size (20,125)
$RadioButton4.Size = New-Object System.Drawing.Size(350,20)
$RadioButton4.Text = 'Administration'
$form.Controls.Add($RadioButton4)

#Création du cinquième radio bouton
$RadioButton5 = New-Object System.Windows.Forms.RadioButton
$RadioButton5.Location =  New-Object System.Drawing.Size (20,150)
$RadioButton5.Size = New-Object System.Drawing.Size(350,20)
$RadioButton5.Text = 'Comptabilité'
$form.Controls.Add($RadioButton5)

#Création de la checkbox pour l'export des dossiers personnels
$Checkbox = New-Object System.Windows.Forms.Checkbox
$Checkbox.Location =  New-Object System.Drawing.Size (20,200)
$Checkbox.Size = New-Object System.Drawing.Size(350,20)
$Checkbox.Text = 'Dossier personnel'
$form.Controls.Add($Checkbox)

#Création du bouton Export
$ExportButton = New-Object System.Windows.Forms.Button
$ExportButton.Location = New-Object System.Drawing.Point(25,250)
$ExportButton.Size = New-Object System.Drawing.Size(200,50)
$ExportButton.Text = 'Export'
$form.Controls.Add($ExportButton)

#Création du bouton Restore
$RestoreButton = New-Object System.Windows.Forms.Button
$RestoreButton.Location = New-Object System.Drawing.Point(250,250)
$RestoreButton.Size = New-Object System.Drawing.Size(200,50)
$RestoreButton.Text = 'Restore'
$form.Controls.Add($RestoreButton)

#Ajout des actions sur les différents boutons
$ExportButton.Add_Click({Button_Export})
$RestoreButton.Add_Click({Button_Restore})

#Affichage de la GUI
$result = $form.ShowDialog()




    






